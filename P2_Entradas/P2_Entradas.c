/*#include <stdio.h>
#include "pico/stdlib.h"
#define BotonA 15
#define LED 5

bool botonA_estado = false;

int main()
{
    stdio_init_all();
    gpio_init(BotonA);
    gpio_init(LED);
    gpio_set_dir(BotonA, GPIO_IN);
    gpio_set_dir(LED, GPIO_OUT);
    while (1)
    {
        botonA_estado = gpio_get(BotonA);
        if (botonA_estado == true)
        {
            gpio_put(LED, 1);
        }
        else
        {
            gpio_put(LED, 0);
        }
    }

    return 0;
}
*/
#include <stdio.h>
#include "pico/stdlib.h"
#define BotonA 14
#define BotonB 15
#define LED1 5

bool botonA_estado = false;
bool botonB_estado = false;

int main()
{
    stdio_init_all();
    gpio_init(LED1);
    gpio_set_dir(LED1, GPIO_OUT);
    gpio_init(BotonA);
    gpio_init(BotonB);
    gpio_set_dir(BotonA, GPIO_IN);
    gpio_set_dir(BotonB, GPIO_IN);
    while (1)
    {
        botonA_estado = gpio_get(BotonA);
        botonB_estado = gpio_get(BotonB);
        if ((botonA_estado == true) && (botonB_estado == true))
        {
            gpio_put(LED1, true);
        }
        else
        {
            gpio_put(LED1, false);
        }
    }
    return 0;
}