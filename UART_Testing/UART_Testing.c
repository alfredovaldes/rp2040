#include <stdio.h>
#include "pico/stdlib.h"

int main()
{
    gpio_set_function(0, GPIO_FUNC_UART);
    gpio_set_function(1, GPIO_FUNC_UART);

    uart_init(uart0, 9600);

    uart_putc_raw(uart0, '1');
    uart_puts(uart0, "Hello, world!");

    return 0;
}
