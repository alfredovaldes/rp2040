/**
 * @file HSU.C
 * @author Alfredo Valdes
 * @date 28 Dec 2021
 * @brief Basic usage of PN532 reader through UART0.
 */

#include "pico/stdlib.h"
#include <stdio.h>
#include <tusb.h>
#include "HSU_NFC/hsu_nfc.h"
#include "hardware/uart.h"
/**
 * @brief Initializes card reader.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, if the value is greater than 0 then the device has been succesfully initialized.
 */
uint8_t HSU_Initialize(void);
/**
 * @brief Initializes card reader and then puts it in a sleep state.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, if the value is greater than 0 then the device has been succesfully initialized and sent to sleep.
 */
uint8_t HSU_Initialize_Sleep(void);
/**
 * @brief Prepares the card reader to read a card.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 */
void HSU_Init_Transaction(void);
/**
 * @brief Wakes up the card reader.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, if the value is greater than 0 then the device has been succesfully awoken.
 */
uint8_t HSU_WakeUp(void);
/**
 * @brief Asks for the firmware version of the card reader. This is a means of insurance that the device is awake and reachable.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, if the value is greater than 0 then the device has been succesfully responded.
 */
uint8_t HSU_GetFwVer(void);
/**
 * @brief Puts the card reader in a sleep state.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, if the value is greater than 0 then the device has been succesfully put to sleep.
 */
uint8_t HSU_Sleep(void);
/**
 * @brief Gets the current status of the tx buffer.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, if the value is greater than 0 then the device is ready to send data.
 */
uint8_t HSU_is_tx_ready(void);
/**
 * @brief Gets the current status of the tx buffer.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, if the value is greater than 0 then the device has finished sending data.
 */
uint8_t HSU_is_tx_done(void);
/**
 * @brief Gets the current status of the rx buffer.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, if the value is greater than 0 then the device is ready to receive data.
 */
uint8_t HSU_is_rx_ready(void);
/**
 * @brief Reads a byte from the UART.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, the value read from the UART.
 */
uint8_t HSU_Read(void);
/**
 * @brief Sends a byte through the UART.
 * @param txData the byte to be sent.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 */
void HSU_Write(uint8_t txData);
/**
 * @brief Calculates a proper checksum for the data to be sent or received.
 * @param varA a pointer to the data buffer.
 * @param sizeA the lenght of the data buffer
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, the proper checksum value.
 */
uint8_t getDcs(uint8_t *varA, int sizeA);
/**
 * @brief Gets authorization to read or write an address in the card.
 * @param address  the card address to be written or read from.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, a value greater than 0 indicates authorization.
 */
uint8_t HSU_AuthUID(uint8_t address);
/**
 * @brief Reads from an specified address in the card.
 * @attention readAddressBuffer will store the received data from the card
 * @param address  the card address to be read from.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, a value greater than 0 indicates a successful reading.
 */
uint8_t HSU_ReadAddress(uint8_t address);
/**
 * @brief Writes to an specified address in the card.
 * @param address  the card address to be written into.
 * @param stringData a pointer to the 16 byte buffer that will be written into the address
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, a value greater than 0 indicates a successful write.
 */
uint8_t HSU_WriteAddress(uint8_t address, const uint8_t *stringData);
/**
 * @brief Wakes up the reader, gets authorization and the makes a read from address 6 and stores the result in readAddressBuffer
 * @attention readAddressBuffer will store the received data from the card
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 */
void int_read_address_6(void);
/**
 * @brief Wakes up the reader, gets authorization and the writes a 16 byte buffer into address 6
 * @param stringData a pointer to the data that will be written into the card
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 */
void int_write_address_6(const uint8_t *stringData);
/**
 * @brief Wakes up the reader, gets authorization and the writes the user, permission and inOrOut data to address 6 in the card, then reads the data and prints it to the usb terminal
 * @param permission indicates if a user has permission to get into the parking lot.
 * @param user a user number. 255 is an invalid user number.
 * @param inOrOut indicates if the user is going in or out. 0 means going in and 1 means going out.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 */
void write_new_card_data_to_address_6(uint8_t permission, uint8_t user, uint8_t inOrOut);
/**
 * @brief Gets the user permission from the read buffer.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number, a value greater than 0 indicates authorization.
 */
uint8_t get_permission(void);
/**
 * @brief Gets the user number from the read buffer.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number.
 */
uint8_t get_user(void);
/**
 * @brief Gets the user inbound or outbound status from the read buffer.
 * @warning This code is blocking, so if the card reader is unresponsive, the microcontroller will be stuck here
 * @return an 8 bit number.
 */
uint8_t get_inOrOut(void);

int main()
{
  stdio_init_all();
  while (!tud_cdc_connected())
  {
    sleep_ms(100);
  }
  sleep_ms(3000);
  // Set the GPIO pin mux to the UART - 0 is TX, 1 is RX
  gpio_set_function(0, GPIO_FUNC_UART);
  gpio_set_function(1, GPIO_FUNC_UART);
  gpio_init(3);
  gpio_set_function(3, GPIO_IN);
  // Inits UART 0
  uart_init(uart0, 115200);
  printf("Inicializando...\n");
InitDeviceLabel:; // Inits card reader
  uint8_t init = HSU_Initialize_Sleep();
  if (init)
  {
    printf("Esperando tarjeta\n");
    while (1)
    {
      bool status = gpio_get(3);
      if (status)
      {
        int_read_address_6();
        uint8_t permission = get_permission();
        uint8_t user = get_user();
        uint8_t inOrOut = get_inOrOut();
        if (permission == 1)
        {
          write_new_card_data_to_address_6(permission, user, inOrOut);
        }
        status = false;
        sleep_ms(10000);
      }
    }
  }
  else
  {
    printf("Fallo :(\n");
    while (1)
    {
      char instruccion = getchar_timeout_us(0);
      switch (instruccion)
      {
      case 'R':
        goto InitDeviceLabel;
        break;
      default:
        break;
      }
    }
  }
  return 0;
}

uint8_t get_user(void)
{
  return readAddressBuffer[0];
}

uint8_t get_permission(void)
{
  return readAddressBuffer[1];
}

uint8_t get_inOrOut(void)
{
  return readAddressBuffer[2];
}

void int_read_address_6(void)
{
  uint8_t init = HSU_Initialize();
  if (init)
  {
    HSU_Init_Transaction();
    uint8_t auth = HSU_AuthUID(6);
    if (auth)
    {
      // Clear write buffer
      for (int i = 0; i < 16; i++)
      {
        readAddressBuffer[i] = 0;
      }
      uint8_t read = HSU_ReadAddress(6);
      if (read)
      {
        printf("Datos leidos:");
        for (int i = 0; i < 16; i++)
        {
          printf(" %02x ", readAddressBuffer[i]);
        }
        printf("\n");
        while (!HSU_Initialize_Sleep())
          ;
      }
      else
      {
        printf("Fallo lectura\n");
      }
    }
    else
    {
      printf("Fallo lectura\n");
    }
  }
  else
  {
    printf("Fallo lectura\n");
  }
  printf("Esperando tarjeta\n");
}

void int_write_address_6(const uint8_t *stringData)
{
  uint8_t init = HSU_Initialize();
  if (init)
  {
    HSU_Init_Transaction();
    uint8_t auth = HSU_AuthUID(6);
    if (auth)
    {
      uint8_t write = HSU_WriteAddress(6, stringData);
      if (write)
      {
        printf("Datos Escritos\n");
        while (!HSU_Initialize_Sleep())
          ;
      }
      else
      {
        printf("Fallo escritura\n");
      }
    }
    else
    {
      printf("Fallo escritura\n");
    }
  }
  else
  {
    printf("Fallo escritura\n");
  }
  printf("Esperando tarjeta\n");
}

void write_new_card_data_to_address_6(uint8_t permission, uint8_t user, uint8_t inOrOut)
{
  uint8_t card_id[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  card_id[0] = user;
  card_id[1] = permission;
  if (inOrOut == 1)
  {
    card_id[2] = 0;
  }
  else
  {
    card_id[2] = 1;
  }
  int_write_address_6(card_id);
  int_read_address_6();
}

uint8_t HSU_is_tx_ready(void)
{
  return uart_is_writable(uart0);
}

uint8_t HSU_is_tx_done(void)
{
  uart_tx_wait_blocking(uart0);
  return true;
}

uint8_t HSU_is_rx_ready(void)
{
  return uart_is_readable(uart0) > 0 ? true : false;
}

uint8_t HSU_Read(void) { return uart_getc(uart0); }

void HSU_Write(uint8_t txData) { uart_putc_raw(uart0, txData); }

uint8_t getDcs(uint8_t *varA, int sizeA)
{
  uint16_t dcs = 0;
  for (int i = 0; i < sizeA; i++)
  {
    dcs = dcs + *varA;
    varA++;
  }
  return dcs & 0xff;
}

/**
 * WAKEUP
 * TX: 55 55 00 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 03 FD D4 14 01 17 00
 * RX: 00 00 FF 00 FF 00 00 00 FF 02 FE D5 15 16 00
 */
uint8_t HSU_WakeUp(void)
{
TryToWake:
  for (int i = 0; i < 24; i++)
  {
    HSU_Write(wkUpCmd[i]);
  }
  uart_read_blocking(uart0, wkUpRes, 15);
  uint8_t rightAns = true;
  for (int i = 0; i < 15; i++)
  {
    if (wkUpRes[i] != wkUpExpectedRes[i])
    {
      rightAns = false;
    }
  }
  return rightAns;
}

/**
 * SLEEP
 * TX: 00 FF 04 FC D4 16 18 01 FD 00
 * RX: 00 00 FF 00 FF 00 00 00 FF 03 FD D5 17 00 14 00
 */
uint8_t HSU_Sleep(void)
{
TryToSleep:
  for (int i = 0; i < 10; i++)
  {
    HSU_Write(sleepCmd[i]);
  }
  uart_read_blocking(uart0, sleepRes, 16);
  uint8_t rightAns = true;
  for (int i = 0; i < 15; i++)
  {
    if (sleepRes[i] != sleepExpectedRes[i])
    {
      rightAns = false;
    }
  }
  return rightAns;
}

/**
 * GET FIRMWARE VERSION
 * TX: 00 FF 02 FE D4 02 2A
 * RX: 00 00 FF 00 FF 00 00 00 FF 06 FA D5 03 32 01 06 07 E8 00
 */
uint8_t HSU_GetFwVer(void)
{
TryToFw:
  for (int i = 0; i < 8; i++)
  {
    HSU_Write(getFwCmd[i]);
  }
  uart_read_blocking(uart0, getFwRes, 19);
  for (int i = 0; i < 14; i++)
  {
    if (getFwRes[i] != getFwExpectedRes[i])
    {
      return false;
    }
  }
  return true;
}

void HSU_Init_Transaction(void)
{
TryToRead:
  for (int i = 0; i < 11; i++)
  {
    HSU_Write(getCardUidCmd[i]);
  }
  for (int i = 0; i < 11; i++)
  {
    getCardUidRes[i] = HSU_Read();
  }
  for (int i = 11; i < 11 + getCardUidRes[9] + 2; i++)
  {
    getCardUidRes[i] = HSU_Read();
  }
  if (getCardUidRes[9] == 0x0c)
  {
    for (int i = 0; i < 4; i++)
    {
      targetUid[i] = getCardUidRes[19 + i];
    }
  }
}

uint8_t HSU_AuthUID(uint8_t address)
{

  uint8_t dcs = 0;
  uint8_t authCmdSyntax[15] = {
      0xD4, 0x40, 0x01, 0x60, address,
      0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
      0xFF, targetUid[0], targetUid[1], targetUid[2], targetUid[3]};
  dcs = 0 - getDcs(authCmdSyntax, 15);
TryToAuth:
  for (int i = 0; i < 8; i++)
  {
    HSU_Write(authHeader[i]);
  }
  HSU_Write(AUTH_COMMAND);
  HSU_Write(address);
  for (int i = 0; i < 6; i++)
  {
    HSU_Write(0xFF);
  }
  for (int i = 0; i < 4; i++)
  {
    HSU_Write(targetUid[i]);
  }
  HSU_Write(dcs);
  HSU_Write(0x00);
  for (int i = 0; i < 16; i++)
  {
    authRes[i] = HSU_Read();
  }
  if (authRes[13] == 0x00)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/*
 * Read Address
 * TX: 00 FF 05 FB D4 40 01 30 06 B5 00
 * RX: 00 00 FF 00 FF 00 00 00 FF 13 ED
 *     D5 41 00 41 6C 66 72 65 64 6F 20
 *     56 61 6C 64 65 73 32 30 4C 00
 */

uint8_t HSU_ReadAddress(uint8_t address)
{
TryToRead:
  for (int i = 0; i < 7; i++)
  {
    HSU_Write(cmdHeader[i]);
  }
  HSU_Write(READ_COMMAND);
  HSU_Write(address);
  uint8_t authCmdSyntax[5] = {0xD4, 0x40, 0x01, READ_COMMAND, address};
  // HSU_Write(0x100 - getDcs(authCmdSyntax, 5));
  HSU_Write(0 - getDcs(authCmdSyntax, 5));
  HSU_Write(0x00);
  uint8_t responseBuffer = 0x00;
  for (int i = 0; i < 14; i++)
  {
    responseBuffer = HSU_Read();
  }
  if (responseBuffer == 0x00)
  {
    for (int i = 0; i < 16; i++)
    {
      readAddressBuffer[i] = HSU_Read();
    }
    for (int i = 0; i < 2; i++)
    {
      uint8_t dummy = HSU_Read();
    }
    return true;
  }
  else
  {
    return false;
  }
}
// 00 00 FF 0F F1 D4 40 01 A0 06 30 31 32 33 34 35 36 37 38 39 41 42 43 44 45 46
// A3 00

uint8_t HSU_WriteAddress(uint8_t address, const uint8_t *stringData)
{
  uint8_t dcs = 0;
  uint8_t writeCmdSyntax[21] = {0xD4, 0x40, 0x01,
                                WRITE_COMMAND, address, stringData[0],
                                stringData[1], stringData[2], stringData[3],
                                stringData[4], stringData[5], stringData[6],
                                stringData[7], stringData[8], stringData[9],
                                stringData[10], stringData[11], stringData[12],
                                stringData[13], stringData[14], stringData[15]};
  // dcs = (uint8_t) (0x100 - getDcs(writeCmdSyntax, 21))&0xff;
  dcs = 0 - getDcs(writeCmdSyntax, 21);
TryToWrite:
  for (int i = 0; i < 7; i++)
  {
    HSU_Write(writeHeader[i]);
  }
  HSU_Write(WRITE_COMMAND);
  HSU_Write(address);
  for (int i = 0; i < 16; i++)
  {
    HSU_Write(stringData[i]);
  }
  HSU_Write(dcs);
  HSU_Write(0x00);
  uint8_t responseBuffer = 0x00;
  for (int i = 0; i < 14; i++)
  {
    responseBuffer = HSU_Read();
  }
  for (int i = 0; i < 2; i++)
  {
    uint8_t dummy = HSU_Read();
  }
  if (responseBuffer == 0x00)
  {
    return true;
  }
  else
  {
    return false;
  }
}

uint8_t HSU_Initialize(void)
{
  uint8_t wakeUp = HSU_WakeUp();
  uint8_t getFwFail = HSU_GetFwVer();
  return wakeUp && getFwFail;
}

uint8_t HSU_Initialize_Sleep(void)
{
  uint8_t wakeUp = HSU_WakeUp();
  uint8_t getFwFail = HSU_GetFwVer();
  uint8_t isAsleep = HSU_Sleep();
  return wakeUp && getFwFail && isAsleep;
}