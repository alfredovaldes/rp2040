#include <stdio.h>
#include "pico/stdlib.h"

int main()
{
    stdio_init_all();
    gpio_init(0);
    gpio_init(1);
    gpio_set_dir(0, true);
    gpio_set_dir(1, true);
    while (1)
    {
        gpio_clr_mask(0);
        gpio_set_mask(3);
        printf("0, 1: 1, 1, %08x\n", gpio_get_all());
        sleep_ms(1000);
        gpio_clr_mask(3);
        gpio_set_mask(2);
        printf("0, 1: 0, 1, %08x\n", gpio_get_all());
        sleep_ms(1000);
        gpio_clr_mask(2);
        gpio_set_mask(1);
        printf("0, 1: 1, 0, %08x\n", gpio_get_all());
        sleep_ms(1000);
        gpio_clr_mask(1);
        gpio_set_mask(0);
        printf("0, 1: 0, 0, %08x\n", gpio_get_all());
        sleep_ms(1000);
    }
    return 0;
}
